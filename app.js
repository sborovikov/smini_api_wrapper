 /*
    yoiDoctoMd plagin https://www.npmjs.com/package/yuidoc2md
    command: readme.md --template node_modules/yuidoc2md/templates/class-with-tables.hbs
*/


var CtApi = require('ct-api');
var async = require('async');
var errors = require('./errors');
var api = new CtApi();
var _ = require('underscore');


/**
 @module smarty-api-wrapper
 */

/**
ApiWrapper provide the extend methods wich wrap pure ct-Api methods.

Also it great what you don't need to call additional properties from the required api constructor. Just do next:

    var api = require('api-wrapper');
    
    api.init({
        baseURL: 'api url there ... ',
        email: '',
        password: ''
    });
    
    api.getUserNotificationColletion(...)  // new methods is available
    api.getObject(..,) // pure method of ctApi is available


and after it you can use required api as is.

    @class ApiWrapper
 */

function ApiWrapper() {

    var self = this;

    // Add websocket pointer
    this.ws = api.ws;

    this.init = function(config, callback) {
        if (api.loggedIn) {
            callback(null, this);
            return;
        }

        api.config(config);

        if (!config.login || !config.password) {
            callback(new errors.LoggedInError('не верные параметры логина и пароля'));
            return;
        }

        api.init(function(initErr) {
            if (initErr) {
                callback(initErr);
                return;
            }

            api.login(config.login, config.password, function (loginErr) {
                if (loginErr) {
                    callback(loginErr);
                    return;
                }

                callback(null, self);
            });
        });
    };
}




var WrProto = ApiWrapper.prototype;

/* PRIVAT METHODS
==============================================================*/

WrProto._getUserObject = function (user, callback) {
    if (user && user._id) {
        callback(null, user);
        return;
    }

    // check if param @user is username or ID
    var checkForHexRegExp = new RegExp('^[0-9a-fA-F]{24}$');

    if (checkForHexRegExp.test(user)) {
        api.getUser(user, callback);
    }
    else {
        api.getUserByName(user, callback);
    }
};


/* PUBLIC METHODS
==============================================================*/


/**
    Returns the group object for a collection of objects of type "notification"

    @method getUserNotificationColletion
    @param user {Object | String}
    @param callback {Function}
**/
WrProto.getUserNotificationColletion = function(user, callback) {
    var self = this;

    async.waterfall([
        function(next) {
            self._getUserObject(user, next);
        },

        function(user, next) {
            api.getObjectChildren(user.homeObject, {levels: [2]}, function(err, res) {
                        if (err) return next(err);

                        for (var i = 0; i < res.length; i++) {
                            if (res[i]._title == 'notifications') {
                                return next(null, res[i]);
                            }
                        }

                        return callback(errors.NotFoundError('collection "notifications" not found'));
                    });
        }
    ], callback);
    
};

/**
    Returns the group object for a collection of objects of type "ChatRoomConfig"

    @method getUserChatRoomCollection
    @param user {Object | String}
    @param callback {Function}
**/
WrProto.getUserChatRoomCollection = function (user, callback) {
    var self = this;

    async.waterfall([
        function(next) {
            self._getUserObject(user, next);
        },

        function (user, next) {
            api.getObjectChildren(user.homeObject, {levels: [2]}, function(err, res) {
                if (err) {
                    return next(err);
                }

                for (var i = 0, len = res.length; i< len; i++) {
                    if (res[i]._title === 'chatRooms') return next(null, (res[i]));
                }

                next(new errors.NotFoundError("collection 'chatRooms' not found"));

            });
        }
    ], callback);
};



/**
Return group object for contacts /homeObject/myContacts/contacts

@method getContactsCollection
@param user {Object|id|username}
@param options {Object} - options for pure api method getObjectChildren
@param callback {Function} arguments: err, contactsObject
*/
WrProto.getContactsCollection = function (user, _options, _callback) {

    var options = (typeof arguments[1] == 'object') ? arguments[1] : {};
    var lastArgument = arguments[arguments.length-1];
    var callback = (typeof lastArgument == 'function') ? lastArgument : 123;

    var self = this;

    async.waterfall([

        function(next) {
            self._getUserObject(user, next);
        },

        function(userObject, next) {
            api.getObjectChild(userObject.homeObject, 'myContacts', next);
        },

        function(myContacts, next) {
            api.getObjectChild(myContacts, 'contacts', next);
        }

    ], callback);
};


WrProto.getAllContacts = function (user, _options, _callback) {

    var options = (typeof arguments[1] === 'object') ? arguments[1] : {};
    var lastArgument = arguments[arguments.length - 1];
    var callback = (typeof lastArgument === 'function') ? lastArgument : 123;

    options.levels = [1];

    var self = this;

    async.waterfall([

        function(next) {
            self.getContactsCollection(user, {}, next);
        },

        function(contacts, next) {
            api.getObjectChildren(contacts, options, next);
        }

    ], callback);
};


/**
method returns selected objects uder chatRooms object

@method queryUserChatRoomConfigs
@param user {Object|id}
@param options {Object} - options for pure api method getObjectChildren
*/
WrProto.queryUserChatRoomConfigs = function (user, options, callback) {
    var self = this;

    async.waterfall([
        function(next) {
            self._getUserObject(user, next);
        },
        function(_user, next) {
            self.getUserChatRoomCollection(_user, next);
        },
        function(res, next) {
            api.getObjectChildren(res, options, next);
        }
    ], callback);
};

/**
    @description
    Create object typeof "notification" in the collection _home/myData/notifications/_

    @method createUserNotification
    @param _user { object|String }
    @param type {string} - template name (set type of the future object)
    @param _options {object} optional hash
        @param {Object} _options.values - values for the future object's fields
        @param {Boolean} _options.spoofOwner If true - the property _createdBy will be spoofed to the user._createdBy
    @param {Function} (err, notification)
**/
WrProto.createUserNotification = function (_user, type, _options, callback) {

    if (!_user || !type) throw new Error('bad argument');

    var defaultOptions = {spoofOwner: false};
    var self = this;
    var user = null;

    var options = (typeof arguments[2] == 'object') ? arguments[2] : {};
    var values = options.values || {};
    _.defaults(options, defaultOptions);

    var lastArgument = arguments[arguments.length - 1];
    callback = (typeof lastArgument == 'function') ? lastArgument: Function;
    

    async.waterfall([
        function(next) {
            self._getUserObject(_user, next);
        },
        function(user_, next) {
            user = user_;
            self.getUserNotificationColletion(user, next);
        },
        function(notificationCollection, next) {
            api.createObject(notificationCollection, {values: values, template: type}, next);
        },
        function(object, next) {
            if ( !options.spoofOwner ) {
                return next(null, object);
            }

            // spoof _createdBy
            api.updateObject(object, {_createdBy: user._id, _owner: user._id}, next);
        }
    ], callback);
};


/**
    Create object typeof chatRooomConfig in the collection _home/myChatRooms/chatRooms/_

    @async
    @method createChatRoomConfigObject
    @param _user { object|id }
    @param _options {Object} An options hash
        @param {Object} _options.values. Hash of values for new object.
        @param {Boolean} _options.spoofOwner If true - the property _createdBy will be spoofed to the user._createdBy
    @param _callback {Function}
    @return {arguments} err, chatRoomConfig
*/
WrProto.createChatRoomConfigObject = function (_user, _options, _callback) {

    if (!_user) throw new Error('bad argument');

    var defaultOptions = {spoofOwner: false};
    var self = this;
    var user = null;

    var options = (typeof arguments[1] == 'object') ? arguments[1] : {};
    var values = options.values || {};
    _.defaults(options, defaultOptions);

    var lastArgument = arguments[arguments.length - 1];
    var callback = (typeof lastArgument == 'function') ? lastArgument: Function;

    
    async.waterfall([
        function(next) {
            self._getUserObject(_user, next);
        },
        function(user_, next) {
            user = user_;
            self.getUserChatRoomCollection(user, next);
        },
        function(chatRoomCollection, next) {
            api.createObject(chatRoomCollection, {values: values}, next);
        },
        function(object, next) {
            if ( !options.spoofOwner ) return next(null, object);
            // spoof _createdBy
            api.updateObject(object, {_createdBy: user._id, _owner: user._id}, next);
        }
    ], callback);
    
};

WrProto.createContact = function(user, options, callback) {

    if (!options) {
        throw new Error('bad argument');
    }

    var self = this;

    async.waterfall([

        function(next) {
            self._getUserObject(user, next);
        },

        function(userObject, next) {
            api.getObjectChild(userObject.homeObject, 'myContacts', function(err, myContacts) {
                next(err, userObject, myContacts);
            });
        },

        function(userObject, myContacts, next) {
            api.getObjectChildren(myContacts, {
                levels: [2],
                query: { _title: 'stage', order: 0 }
            }, function(err, stages) {
                next(err, userObject, myContacts, stages);
            });
        },

        function(userObject, myContacts, stages, next) {
            api.getObjectChild(myContacts, 'contacts', function(err, contactsObject) {
                next(err, userObject, contactsObject, stages);
            });
        },

        function(userObject, contactsObject, stages, next) {
            options.values.stage = stages[0] && stages[0]._id;
            options.values.group = 0;

            api.createObject(contactsObject, options, next);
        },

        function(object, next) {
            if ( !options.spoofOwner ) {
                return next(null, object);
            }
            // spoof _createdBy
            api.updateObject(object, {_createdBy: user._id, _owner: user._id}, next);
        }

    ], callback);

};

// Exclude init method
var apiMethods = _.without(api._getAsyncMethodNames(), 'init');

for (var i = 0, len = apiMethods.length; i < len; i++) {
    var methodName = apiMethods[i];
    ApiWrapper.prototype[methodName] = api[methodName].bind(api);
}


module.exports = new ApiWrapper();
