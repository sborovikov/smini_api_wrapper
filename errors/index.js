require('extend-error');

module.exports.ArgumentsError = Error.extend('ArgumentsError', 500);
module.exports.NotFoundError = Error.extend('NotFoundError', 404);
module.exports.LoggedInError = Error.extend('LoggedInError', 500);