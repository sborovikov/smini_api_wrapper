#smarty-api-wrapper/ApiWrapper

ApiWrapper provide the extend methods wich wrap pure ct-Api methods.

Also it great what you don't need to call additional properties from the required api constructor. Just do next:

	var api = require('api-wrapper');
	
	api.init({
		baseURL: 'api url there ... ',
		email: '',
		password: ''
	}); 
	
	api.getUserNotificationColletion(...)  // new methods is available
	api.getObject(..,) // pure method of ctApi is available


and after it you can use required api as is.

##Methods

###getUserNotificationColletion

Returns the group object for a collection of objects of type &quot;notification&quot;

**Params**:  
*   user _Object | String_

    
*   callback _Function_

    


###getUserChatRoomCollection

Returns the group object for a collection of objects of type &quot;ChatRoomConfig&quot;

**Params**:  
*   user _Object | String_

    
*   callback _Function_

    


###createUserNotification

Create object typeof &quot;notification&quot; in the collection _home/myData/notifications/_

**Params**:  
*   _user _Object|String_

    
*   type _String_

    - template name (set type of the future object)
*   _options _Object_

    optional hash
*   (err, _Function_

    notification)


###createChatRoomConfigObject

Create object typeof chatRooomConfig in the collection _home/myChatRooms/chatRooms/_

**Returns**: _Arguments_ - err, chatRoomConfig

**Params**:  
*   _user _Object_

    
*   _options _Object_

    An options hash
*   _callback _Function_

    


