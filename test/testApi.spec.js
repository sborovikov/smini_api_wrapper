require('mocha');
var should = require('should');
var api = require('../app');
var async = require('async');

var CONFIG = {
                baseURL: 'http://mini-dev.u-kon.ru',
                login: 'admin',
                password: '123123'
            };

var mockUser = null;


function clearMockData(callback) {
    api.getUserByName('mock@test.com', function(err, user) {
        if (err) return callback();
        api.deleteUser(user, callback);
    });
}

describe('Тест кастомных АПИ методов.', function() {
    before(function(done) {

        async.series([
            function initApi (next) {
                api.init(CONFIG, next);
            },
            function clearMock(next) {
                clearMockData(next);
            },
            function createMockUser (next) {
                api.createUser({email: 'mock@test.com', password: '123123'}, function(err, user) {
                    if (err) {
                        next(err);
                        return;
                    }
                    mockUser = user;
                    next();
                });
            }
        ], done);
    });


    after(function(done) {
        clearMockData(done);
    });


    describe('#_getUserObject', function() {
        it('Должен найти пользователя по email', function(done) {
            api._getUserObject('mock@test.com', function(err, user) {
                if (err) {
                    console.error(err);
                    done(err);
                    return;
                }

                user.email.should.be.equal('mock@test.com');
                done();
            });
        });
    });


    describe('#getUserNotificationColletion', function() {
        it(' should return object with _title = "notifications"', function(done) {
            api.getUserNotificationColletion(mockUser, function(err, res) {
                if (err) return done(err);
                res._title.should.be.equal('notifications');
                done();
            });
        });
    });
    

    describe('#getUserChatRoomCollection', function() {
        it('Должен вернуть объект с _title = "chatRooms"', function(done) {
            api.getUserChatRoomCollection(mockUser, function(err, res) {
                if (err) return done(err);
                res._title.should.be.equal('chatRooms');
                done();
            });
        }); 
    });


    describe('#createChatRoomConfigObject', function () {
        var mockedObjects = [];

        after(function(done) {
            async.eachSeries(mockedObjects, api.deleteObject.bind(api), done);
        });

        it('Должен создать корректный объект chatRoomConfig в коллекции chatRooms', function (done) {
            api.createChatRoomConfigObject(mockUser, {spoofOwner: true, values: {notify: true}}, function(err, res) {
                if (err) {
                    console.error(err);
                    done(err);
                    return;
                }
                mockedObjects.push(res);
                res._title.should.be.equal('chatRoomConfig');
                res._createdBy.should.be.equal(mockUser._id);
                done();
            });
        });


        it('Должен создать объект chatRoomConfig с notify = true ', function(done) {
            api.createChatRoomConfigObject(mockUser, {values: {notify: true}}, function(err, res) {
                if (err) {
                    console.error(err);
                    done(err);
                    return;
                }

                mockedObjects.push(res);
                res._title.should.be.equal('chatRoomConfig');
                res.notify.should.be.equal(true);
                done();
            });
        });
    });

    describe('#createUserNotification', function () {
        var mockedObjects = [];

        after(function(done) {
            async.eachSeries(mockedObjects, api.deleteObject.bind(api), done);
        });

        it('Должен создать корректный объект notification в коллекции notifications', function (done) {
            api.createUserNotification(mockUser, 'notificationNewChatMessage', {spoofOwner: true}, function(err, res) {
                if (err) {
                    console.error(err);
                    return done(err);
                }
                mockedObjects.push(res);
                res._title.should.be.equal('notificationNewChatMessage');
                res._createdBy.should.be.equal(mockUser._id);
                done();
            });
        });
    });

    describe('#getContactsCollection', function () {
        it('Должен вернуть объект с _title = "contacts"', function (done) {
            api.getContactsCollection(mockUser, function(err, res) {
                if (err) return done(err);
                res._title.should.be.equal('contacts');
                done();
            });
        });
    });

    describe('#createContact', function () {
        var mockedObjects = [];
        var mocketValues = {
            eMail: 'test@test.com',
            manName: 'Test Test'
        };

        after(function(done) {
            async.eachSeries(mockedObjects, api.deleteObject.bind(api), done);
        });

        it('Должен создать корректный объект contact в коллекции contacts', function (done) {
            api.createContact(mockUser, {spoofOwner: true, values: mocketValues}, function(err, res) {
                if (err) {
                    console.error(err);
                    return done(err);
                }
                mockedObjects.push(res);

                res._title.should.be.equal('contact');
                res.eMail.should.be.equal(mocketValues.eMail);
                res.manName.should.be.equal(mocketValues.manName);
                res.stage.should.not.be.equal('');
                res._createdBy.should.be.equal(mockUser._id);
                done();
            });
        });
    });

    describe('#getAllContacts', function () {
        var mockedObjects = [];
        var mocketValues = {
            eMail: 'test@test.com',
            manName: 'Test Test'
        };

        after(function(done) {
            async.eachSeries(mockedObjects, api.deleteObject.bind(api), done);
        });

        it('Должен создать корректный объект contact в коллекции contacts', function (done) {
            api.createContact(mockUser, {spoofOwner: true, values: mocketValues}, function(err, res) {
                if (err) {
                    console.error(err);
                    return done(err);
                }
                mockedObjects.push(res);

                res._title.should.be.equal('contact');
                res.eMail.should.be.equal(mocketValues.eMail);
                res.manName.should.be.equal(mocketValues.manName);
                res.stage.should.not.be.equal('');
                res._createdBy.should.be.equal(mockUser._id);
                done();
            });
        });

        it('Должен вернуть массив с одним объектов у которого _title = "contact"', function (done) {
            api.getAllContacts(mockUser, function(err, res) {
                if (err) {
                    console.error(err);
                    return done(err);
                }
                res.should.be.instanceof(Array).and.have.lengthOf(1);
                res[0]._title.should.be.equal('contact');
                done();
            });
        });

    });

});
